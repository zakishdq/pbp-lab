from django.db import models

# Create your models here.
class Note(models.Model):
    # Complete missing attributes in Friend model
    to_text = models.CharField(max_length=100)
    from_text = models.CharField(max_length=100)
    title_text = models.CharField(max_length=100)
    message_text = models.CharField(max_length=100)