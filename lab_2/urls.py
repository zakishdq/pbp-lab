from django.urls import path
from .views import index, xml
from .views import json

urlpatterns = [
    path('', index, name='index'),
    # Add friends path using friend_list Views
    path('xml', xml, name='xml'),
    path('json', json, name='json')
]