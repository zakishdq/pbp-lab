from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from lab_2.models import Note
from .forms import NoteForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    notes = Note.objects.all().values()  # Implement this to call all friend object
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

@login_required(login_url='/admin/login/')
def add_note(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NoteForm(request.POST or None)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            form.save()
            return HttpResponseRedirect('/lab-4/')

    # if a GET (or any other method) we'll create a blank form
    else:
       form = NoteForm()

    return render(request, 'lab4_form.html', {'form': form})
    
@login_required(login_url='/admin/login/')
def note_list(request):
    notes = Note.objects.all().values()  # Implement this to call all friend object
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)