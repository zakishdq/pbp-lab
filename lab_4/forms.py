from django import forms
from lab_2.models import Note
from django.forms.widgets import NumberInput
import datetime

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = "__all__"