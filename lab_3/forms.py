from django import forms
from lab_1.models import Friend
from django.forms.widgets import NumberInput
import datetime

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'DOB']
    error_messages = {
        'required' : 'Please Type'
    }

    input_attrs = {
        'type' : 'text',
        'placeholder' : 'Your Name'
    }

    name = forms.CharField(label='Nama ',
    max_length=27, widget=forms.TextInput(attrs=input_attrs))

    npm = forms.IntegerField(label='NPM ',
    widget=forms.TextInput(attrs= {'placeholder' : 'Your Student Number'}))

    DOB = forms.DateField(label='Tanggal Lahir ', initial=datetime.date.today,
    widget=NumberInput(attrs={'type': 'date'}))
