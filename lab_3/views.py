from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required
# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()  # Implement this to call all friend object
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)
    
@login_required(login_url='/admin/login/')
def add_friend(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = FriendForm(request.POST or None)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            form.save()
            return HttpResponseRedirect('/lab-3/')

    # if a GET (or any other method) we'll create a blank form
    else:
       form = FriendForm()

    return render(request, 'lab3_form.html', {'form': form})

    