from .models import Friend
from django.shortcuts import render
from datetime import datetime, date

# Complete this data (Name, BOD, npm)
mhs_name = 'Muhammad Zaki Ash-Shidiqi' 
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002, 6, 24)  
npm = 2006595740  


def index(request):
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all().values()  # Implement this to call all friend object
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)
