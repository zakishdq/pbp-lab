from django.db import models

# Create Friend model that contains name, npm, and DOB (date of birth)

class Friend(models.Model):
    # Complete missing attributes in Friend model
    name = models.CharField(max_length=30)
    npm = models.IntegerField()
    DOB = models.DateField()
    
