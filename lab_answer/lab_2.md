# Lab 2: Perbedaan JSON dan XML; Perbedaan HTML dan XML 

## Perbedaan JSON dan XML
JSON adalah singkatan dari _JavaScript Object Notation_. JSON diturunkan dari bahasa pemrogaman javascript. Implementasi penulisan dari JSON mirip seperti Dictionary pada Python dan HashMap pada Java. Untuk tiap elemen pada suatu model dibuat key dan value. Sedangkan XML adalah extensible markup language yang dirancang untuk menyimpan data. Data XML disimpan sebagai tree structure serta XML didesain self-descriptive. Implementasinya mirip HTML yang diawali dan diakhiri dengan tag.

### Berikut adalah beberapa perbedaan mendasar antara keduanya:

1. Data pada JSON disimpan seperti map dengan pasangan, sedangkan data XML disimpan sebagai tree structure
2. Objek pada JSON memiliki type, sedangkan data pada XML bersifat Typeless
3. JSON lebih mudah dibaca oleh manusia, sedangkan XML relatif lebih sulit untuk dibaca
4. JSON kurang aman dibandingkan dengan XML
5. JSON berasal dari bahasa poemrogaman javascript. Sedangkan XML dari SGML (Standard Generalized Markup Language)
6. JSON tidak memiliki kemampuan untuk melakuka display, sedangkan XML bisa.
7. JSON hanya men-support tipe data teks dan angka. Sedangkan XML. Sedangkan XML men-support berbagai tipe data seperti angka, teks, gambar, bagan, grafik, dll.
8. JSON tidak men-support comment, sedangkan XML memperbolehkan comment


## Perbedaan HTML dan XML
HTML adalah singkatan dari _Hyper Text Markup Language_. Tujuan dari HTML adalah untuk menampilkan data dan fokus pada tampilan data. Konten dari HTML mendeskripsikan struktur dari sebuah halaman web. HTML berisi beberapa macam elemen yang akan menyampaikan informasi kepada browser bagaimana untuk menampilkan konten. Sedangkan XML adalah extensible markup language yang merupakan markup language sederhana yang digunakan untuk menyimpan/mentransfer data. XML berfokus pada menyimpan, dan mentransfer data dibanding untuk menampilkannya.

### Berikut adalah beberapa perbedaan mendasar antara keduanya:

1. HTML adalah suatu markup language, sedangkan XML adalah standard markup language yang bisa mendefinisikan markup language lainnya.
2. HTML berfokus pada bagaimana men-display data, sedangkan XML berfokus pada menyimpan/mentransfer data.
3. Tag pada HTML sudah didefinisikan, sedangkan tag pada XML didefinisikan sendiri oleh user.
4. HTML tidak case sensitive, sedangkan XML case sensitive.
5. HTML bersifat static, sedangkan XML dinamic.
6. Closing tag pada HTML tidak wajib, sedangkan pada XML wajib.
7. HTML bisa mengabaikan small error, sedangkan XML tidak memperbolehkan error.


